package com.interrap.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.interrap.app.model.User;
import com.interrap.app.service.ILoginService;

@Controller
@RequestMapping("/auth")
public class LoginController 
{
	private ILoginService loginService;
	
	@Autowired
	public void setLoginService(ILoginService loginService)
	{
		this.loginService = loginService;
	}

	@GetMapping(value="")
	public String getLogin(Model model)
	{		
		model.addAttribute("usuario",new User());
		return "login";
	}

	@PostMapping(value="")
	public String postLogin(@Valid @ModelAttribute User usuario,Model model,HttpServletRequest request,BindingResult bindingResult)
	{
		String pagina;
		User user=loginService.Loguearse(usuario);
		HttpSession session=request.getSession();
		session.setAttribute("username",user.Usuario );
		session.setAttribute("idRol",user.IdRol );
		if(bindingResult.hasErrors())
		{
			pagina="/auth";
		}
		else
		{
			model.addAttribute("user",user);
			pagina= "index";
		}
		
		
		return pagina;
		
	}
}

