package com.interrap.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.interrap.app.model.User;
import com.interrap.app.service.IUserServices;

@Controller
@RequestMapping("/dusers")
public class UsersController
{
		private IUserServices userServices;
		
		@Autowired
		public void setUsersService(IUserServices userServices)
		{
			this.userServices=userServices;
		}
		@GetMapping(value="/new")
		public String newUser(Model model) 
		{
			model.addAttribute("user", new User());
			return "newUser";
		}
		
		@GetMapping(value="")
		public String getUsuarios(Model model) {
			String pagina;
			List<User>lUsers=userServices.GetUsers();
			model.addAttribute("Usuarios",lUsers);
			if(lUsers.isEmpty())
			{
				pagina="403";
			}
			else
			{
				pagina="getUsers";
			}
			return pagina;
		}
		
	
}
