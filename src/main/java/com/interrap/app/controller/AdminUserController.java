package com.interrap.app.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.interrap.app.model.User;
import com.interrap.app.service.IUsuarioService;

@Controller
@RequestMapping("/admin/users") //->/cargos, ->/cargos/12
public class AdminUserController
{
		
	private IUsuarioService usuarioService;
	
	@Autowired
	public void setUsuariosService(IUsuarioService usuarioService) 
	{
	   this.usuarioService = usuarioService;
	}
	
	@GetMapping(value="/new")
	public String newUser(Model model) {
		model.addAttribute("user", new User());
		return "newUser";
	}
	
	@GetMapping(value="")
	public String getUsuarios(Model model) {
		String pagina;
		List<User>lUsuarios=usuarioService.ObtenerUsuarios();
		model.addAttribute("Usuarios",lUsuarios);
		if(lUsuarios.isEmpty())
		{
			pagina="403";
		}
		else
		{
			pagina="getUsers";
	}
		return pagina;
	}
	
	@PostMapping(value="")
	public String Guardar(@ModelAttribute User usuario)
	{
		String pagina;
		if(usuarioService.GuardarUsuario(usuario))
		{
			pagina= "redirect:/admin/users";
		}
		else
		{
			pagina="403";
		}
		return pagina;
	}
	
	@GetMapping("/remove")
	public String DeleteUser(@RequestParam(name="id",required=true) int id,Model model)
	{
		String pagina;
		model.addAttribute(id);
		if(usuarioService.DeleteUser(id))
		{
			pagina="redirect:/admin/users";
		}
		else
		{
			pagina="/error/403";
		}
		return pagina;
	}
	
	@RequestMapping("/edit/{id}")
	public String GetUserById(@PathVariable(name="id") int id, Model model)
	{
		model.addAttribute("id");
		User user=usuarioService.GetUserById(id);
		model.addAttribute("userModel",user);
		return "editUser";
	}
	
	@PostMapping("/edit")
	public String UpdateUser(@ModelAttribute User user)
	{
		String pagina;
		
		if(usuarioService.UpdateUser(user))
		{
			pagina="redirect:/admin/users";
		}
		else
		{
			pagina="error/403";
		}
		return pagina;
	}
}
