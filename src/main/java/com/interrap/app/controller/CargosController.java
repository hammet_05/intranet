package com.interrap.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.interrap.app.model.cargos;
import com.interrap.app.service.CargoService;

@Controller
@RequestMapping("/cargos") // ->/cargos, ->/cargos/12

public class CargosController {

	private CargoService cargoService;

	@Autowired
	public void setCargoService(CargoService cargoService) {
		this.cargoService = cargoService;
	}

	@GetMapping(value = "")
	public String getCargos(Model model) {

		List<String> lista = cargoService.ObtenerCargos();
		model.addAttribute("cargos", lista);
		return "cargos";
	}

	/*@GetMapping(value = "/{id}")
	public String getCargo(@PathVariable(name = "id") Integer _id, Model model) {

		List<String> lista = cargoService.getCargo();
		model.addAttribute("cargos", lista);
		return "cargos";
	}*/

	@PostMapping(value = "")
	public boolean Guardar(@ModelAttribute cargos cargo) {
		return true;
	}
}
