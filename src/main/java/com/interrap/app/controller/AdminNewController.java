package com.interrap.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.interrap.app.model.News;
import com.interrap.app.service.INewsServices;

@Controller
@RequestMapping("/admin/news")
public class AdminNewController {

private INewsServices newService;
	
	@Autowired
	public void setNewsServices(INewsServices newService)
	{
		 this.newService=newService;
	}
	
	@GetMapping(value="/add")
	public String addNews(Model model)
	{
		model.addAttribute("news",new News());
		return "addNews";
	}
	
	@GetMapping(value="")
	public String getNews(Model model)
	{
		String pagina;
		List<News>lNews=newService.GetNews();
		model.addAttribute("News",lNews);
		if(lNews.isEmpty())		
		{
			pagina="error/403";
		}
		else
		{
			pagina="getNews";
		}
		return pagina;
	}
	
	@PostMapping(value="")
	public String SaveNews(@ModelAttribute News _news)
	{
		String pagina;
		if(newService.SaveNews(_news))
		{
			pagina= "redirect:/admin/news";
		}
		else
		{
			pagina="403";
		}
		return pagina;
	}
	
	@RequestMapping("/edit/{id}")
	public String GetNewsById(@PathVariable int id, Model model)
	{
		//model.addAttribute("id", id);
		News news=newService.GetNewsById(id);		
		model.addAttribute("newsModel",news);
		news.setIdNoticia(id);
		return "editNews";
	}
	
	@PostMapping(value="/edit")
	public String UpdateNew(@ModelAttribute News newsModel)
	{
		String pagina;
		if(newService.UpdateNews(newsModel))
		{
			pagina= "redirect:/admin/news";
		}
		else
		{
			pagina="403";
		}
		return pagina;
		
	}
	
}
