package com.interrap.app.model;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User 
{
	@NotNull
	@Digits(integer=6,fraction=0)
	public int Identificacion;
	//"{javax.validation.constraints.Digits.message}";
	@NotNull
	@Size(min=3, max=20)
	public String Usuario;
	@NotNull
	public String Contrasena;
	@NotNull	
	public int IdRol;
	
	public boolean Estado;
	public String Rol;
	

	public User() {	}
	public User(int identificacion)
	{
		Identificacion = identificacion;
	}
	public User(int identificacion, String usuario, String contrasena, int idRol, boolean estado) 
	{
	
	Identificacion = identificacion;
	Usuario = usuario;
	Contrasena = contrasena;
	IdRol = idRol;
	Estado = estado;
	}

//region getters and setters
	public int getIdentificacion() {
		return Identificacion;
	}
	public void setIdentificacion(int identificacion) {
		Identificacion = identificacion;
	}
	public String getUsuario() {
		return Usuario;
	}
	public void setUsuario(String usuario) {
		Usuario = usuario;
	}
	public String getContrasena() {
		return Contrasena;
	}
	public void setContrasena(String contrasena) {
		Contrasena = contrasena;
	}
	public int getIdRol() {
		return IdRol;
	}
	public void setIdRol(int idRol) {
		IdRol = idRol;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	public String getRol() {
		return Rol;
	}
	public void setRol(String rol) {
		Rol = rol;
	}
	//endregion
	
	
}
