package com.interrap.app.model;

import java.util.Date;

public class News 
{
	public int IdNoticia;
	public String FechaInicio;
	public String Titulo;
	public String Descripcion;
	public int IdCreador;
	public String FechaCierre;
	public boolean Estado;
	public String Usuario;
	public int IdCategoria;
	public String Categoria;
	
	public int getIdNoticia() {
		return IdNoticia;
	}
	public void setIdNoticia(int idNoticia) {
		IdNoticia = idNoticia;
	}
	public String getFechaInicio() {
		return FechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}
	public String getTitulo() {
		return Titulo;
	}
	public void setTitulo(String titulo) {
		Titulo = titulo;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public int getIdCreador() {
		return IdCreador;
	}
	public void setIdCreador(int idCreador) {
		IdCreador = idCreador;
	}
	public String getFechaCierre() {
		return FechaCierre;
	}
	public void setFechaCierre(String fechaCierre) {
		FechaCierre = fechaCierre;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	public String getUsuario() {
		return Usuario;
	}
	public void setUsuario(String usuario) {
		Usuario = usuario;
	}
	public int getIdCategoria() {
		return IdCategoria;
	}
	public void setIdCategoria(int idCategoria) {
		IdCategoria = idCategoria;
	}
	public String getCategoria() {
		return Categoria;
	}
	public void setCategoria(String categoria) {
		Categoria = categoria;
	}

	
	

}
