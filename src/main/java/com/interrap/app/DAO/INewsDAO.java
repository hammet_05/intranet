package com.interrap.app.DAO;

import java.util.List;

import com.interrap.app.model.News;

public interface INewsDAO 
{
	List<News>GetNews();
	boolean SaveNews(News news);
	News GetNewsById(int id);
	boolean UpdateNew(News news);
	
}
