package com.interrap.app.DAO;

import java.util.List;

import com.interrap.app.model.User;

public interface IUsuariosDAO
{
	List<User>ObtenerUsuarios();	
	boolean GuardarUsuario(User usuario);
	User GetUserById(int id);
	boolean DeleteUser(int id);
	boolean UpdateUser(User user);
	
}
