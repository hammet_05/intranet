package com.interrap.app.DAO;

import java.util.List;

import com.interrap.app.model.User;

public interface IUserDAO
{
	List<User>GetUsers();
	boolean SaveUser(User user);
	User GetUserById(int id);
	boolean DeleteUser(int id);
}
