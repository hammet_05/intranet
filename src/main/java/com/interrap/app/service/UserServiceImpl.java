package com.interrap.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.interrap.app.model.User;
import com.interrap.app.DAO.IUserDAO;

@Service
public class UserServiceImpl implements IUserServices 
{
	private IUserDAO iUserDao;
	
	@Override
	public List<User> GetUsers() 
	{		
		return iUserDao.GetUsers();
	}

	@Override
	public boolean SaveUser(User user)
	{
		boolean saveUser=false;
		if(iUserDao.SaveUser(user))
		{
			saveUser=true;
		}
		return saveUser;
	}

	@Override
	public User GetUserById(int id) 
	{
		User user = new User();
		user=iUserDao.GetUserById(id);		
	    return user;
	}

	@Override
	public boolean UpdateUser(User user) {
		// TODO Auto-generated method stub
		return false;
	}

}
