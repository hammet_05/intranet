package com.interrap.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interrap.app.DAO.ICargoDAO;

@Service
public class CargoServiceImpl implements CargoService {
	
	@Autowired
	private ICargoDAO dao;

	@Override
	public List<String> ObtenerCargos() 
	{
		return dao.ObtenerCargos();
	}

}
