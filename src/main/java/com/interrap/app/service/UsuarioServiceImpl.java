package com.interrap.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interrap.app.DAO.IUsuariosDAO;
import com.interrap.app.model.User;
@Service
public class UsuarioServiceImpl implements IUsuarioService
{
	@Autowired
	private IUsuariosDAO iUsuarioDao;
	
	@Override
	public List<User> ObtenerUsuarios() 
	{
		
		return iUsuarioDao.ObtenerUsuarios();
	}

	@Override
	public boolean GuardarUsuario(User usuario) 
	{
		boolean guardo=false;
		if(iUsuarioDao.GuardarUsuario(usuario))
		{
			guardo=true;
		}
		
		return guardo;
	}
	@Override
	public User GetUserById(int id)
	{
		User user = iUsuarioDao.GetUserById(id);		
	   return user;
	}

	@Override
	public boolean DeleteUser(int id) {
		
		return iUsuarioDao.DeleteUser(id);
	}

	
	@Override
	public boolean UpdateUser(User user) {
		
		return iUsuarioDao.UpdateUser(user);
	}

}
