package com.interrap.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interrap.app.DAO.ILoginDAO;
import com.interrap.app.model.User;

@Service
public class LoginServiceImpl implements ILoginService
{
	@Autowired
	private ILoginDAO iLoginDAO;
	
	@Override
	public User Loguearse(User user) {

		return iLoginDAO.Loguearse(user);
	}

}
