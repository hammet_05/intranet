package com.interrap.app.service;

import java.util.List;

import com.interrap.app.model.User;

public interface IUserServices 
{
	List<User>GetUsers();
	boolean SaveUser(User user);
	User GetUserById(int id);
	boolean UpdateUser(User user);
}
