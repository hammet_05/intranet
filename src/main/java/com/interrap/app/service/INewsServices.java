package com.interrap.app.service;

import java.util.List;


import com.interrap.app.model.News;

public interface INewsServices 
{
	boolean SaveNews(News _new);
	List<News> GetNews();
	News GetNewsById(int id);
	boolean UpdateNews(News news);
}
