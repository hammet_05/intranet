package com.interrap.app.service;

import java.util.List;

import com.interrap.app.model.User;

public interface IUsuarioService 
{
	List<User>ObtenerUsuarios();
	boolean GuardarUsuario(User usuario);
	User GetUserById(int id);
	boolean DeleteUser(int id);
	boolean UpdateUser(User user);
}
