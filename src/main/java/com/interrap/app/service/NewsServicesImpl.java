package com.interrap.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interrap.app.DAO.INewsDAO;
import com.interrap.app.model.News;
@Service
public class NewsServicesImpl  implements INewsServices
{
	@Autowired
	private INewsDAO iNewsDao;
	

	@Override
	public List<News> GetNews() {
		
		return iNewsDao.GetNews();
	}
	@Override
	public boolean SaveNews(News _new)
	{
	
		return iNewsDao.SaveNews(_new);
	}
	@Override
	public News GetNewsById(int id) 
	{	
		return iNewsDao.GetNewsById(id);
	}
	
	@Override
	public boolean UpdateNews(News news) {
		return iNewsDao.UpdateNew(news);
	}

}
