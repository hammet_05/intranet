package com.interrap.app.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.interrap.app.DAO.INewsDAO;
import com.interrap.app.model.News;
import com.interrap.app.model.User;
@Repository
public class NewsDAOImpl implements INewsDAO
{
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<News> GetNews() 
	{
		String sql="SELECT n.Id_noticia,DATE_FORMAT(n.Fecha_noticia,'%d-%m-%Y') 'Fecha_Noticia',cn.Categoria,n.Titulo_noticia,n.Descripcion,DATE_FORMAT(n.Fecha_cierre,'%d-%m-%Y')'Fecha_Cierre',ra.Usuario,case when n.Estado=1 then 'Activa' else 'Inactiva' end Estado " + 
				" FROM noticia n" + 
				" join rol_acceso ra on n.Id_creador=ra.Id_llave\r\n" + 
				" join not_categoria_noti cn on n.Id_categoria_noti=cn.Id ";
		List lNews=jdbcTemplate.queryForList(sql);
		return lNews;
	}

	@Override
	public boolean SaveNews(News news)
	{
		boolean saveNew=false;
//		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");			
//		String dt=format.format(news.FechaInicio);
		
		String sql="INSERT INTO  noticia " +
					"( Fecha_noticia , Id_categoria_noti , Titulo_noticia , Id_creador ,Fecha_cierre , Descripcion )"+
					"  VALUES ('"+ news.FechaInicio +"',"+ news.IdCategoria + ",'"+news.Titulo +"',"+news.IdCreador +",'"
								+ news.FechaCierre +"','"+news.Descripcion+"')";
	   int registro=jdbcTemplate.update(sql);
	   if(registro>0) 
	   {
		   saveNew=true;   
	   }
		
		return saveNew;
	}

	
	
	@Override
	public News GetNewsById(int id) {
		
		String sql="SELECT n.Id_noticia,DATE_FORMAT(n.Fecha_noticia,'%d-%m-%Y') 'Fecha_Noticia',cn.Id,cn.Categoria,n.Titulo_noticia,n.Descripcion"
				+  ",DATE_FORMAT(n.Fecha_cierre,'%d-%m-%Y')'Fecha_Cierre',ra.Usuario,case when n.Estado=1 then 'Activa' else 'Inactiva' end Estado\r\n" + 
				" FROM noticia n\r\n" + 
				" join rol_acceso ra on n.Id_creador=ra.Id_llave\r\n" + 
				" join not_categoria_noti cn on n.Id_categoria_noti=cn.Id \r\n" + 
				" where n.Id_noticia=?";
		return jdbcTemplate.queryForObject(sql, new GetNewsIdRowMapper(),id);
	}
	
	
	public boolean UpdateNew(News news)
	{
		boolean update=false;
		
		String sql="UPDATE noticia "+
					 "SET Fecha_noticia = '"+ news.FechaInicio +"', " + 
					 "Id_categoria_noti = " + news.IdCategoria +"	, " + 
					 "Titulo_noticia = '" + news.Titulo +"', " + 
					 "Descripcion = '" + news.Descripcion +"', " + 
					 "Estado = " + news.Estado +"," + 
					 "Fecha_cierre = '" + news.FechaCierre +"' " + 
					 "WHERE Id_noticia = "+news.IdNoticia;

		
				if(jdbcTemplate.update(sql)>0)
				{
					update=true;
				}
				return update;
	}
}

class GetNewsIdRowMapper implements RowMapper<News>
{
	@Override
	public News mapRow(ResultSet rs,int i) throws  SQLException
	{
		News news=new News();
		
		news.setFechaInicio(rs.getString("Fecha_noticia"));
		news.setIdCategoria(rs.getInt("Id"));
		news.setCategoria(rs.getString("Categoria"));
		news.setTitulo(rs.getString("Titulo_noticia"));
		news.setDescripcion(rs.getString("Descripcion"));
		news.setFechaCierre(rs.getString("Fecha_cierre"));
		news.setUsuario(rs.getString("Usuario"));
		news.setEstado(rs.getBoolean("Estado"));
		return news;
	}
}
