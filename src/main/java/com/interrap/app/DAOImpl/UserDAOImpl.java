package com.interrap.app.DAOImpl;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.interrap.app.DAO.IUserDAO;
import com.interrap.app.model.User;

@Repository
public class UserDAOImpl implements IUserDAO 
{
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource)
	{
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
	@Override
	
	
	public List<User> GetUsers() {
		String sql = "SELECT ra.Id_llave,ra.Usuario,r.Nombre_rol,CASE WHEN ra.Estado=1 THEN 'Activo' ELSE 'Inactivo' END Estado\r\n"
				+ "FROM rol_acceso ra\r\n" + "JOIN rol r ON ra.id_rol=r.Id_rol";

		List lUsers = jdbcTemplate.queryForList(sql);
		return lUsers;

	}

	@Override
	public boolean SaveUser(User user) {
		
		return false;
	}

	@Override
	public User GetUserById(int id) {
		
		return null;
	}
	@Override
	public boolean DeleteUser(int id) {
		boolean deleteUser=false;
		String sql="UPDATE rol_acceso set estado=0 where Id_llave= "+ id;
		
		return false;
	}

}
