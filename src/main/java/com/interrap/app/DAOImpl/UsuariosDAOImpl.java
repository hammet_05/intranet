package com.interrap.app.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.interrap.app.DAO.IUsuariosDAO;
import com.interrap.app.model.User;

@Repository
public class UsuariosDAOImpl implements IUsuariosDAO
{
	
	private JdbcTemplate jdbcTemplate;
	
	
	@Autowired
	public void setDataSource(DataSource dataSource)
	{
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
	
	

	@Override
	public List<User> ObtenerUsuarios() {
	
		
		String sql="SELECT ra.Id_llave,ra.Usuario,r.Nombre_rol,CASE WHEN ra.Estado=1 THEN 'Activo' ELSE 'Inactivo' END Estado\r\n" + 
				"FROM rol_acceso ra\r\n" + 
				"JOIN rol r ON ra.id_rol=r.Id_rol";
				  
		List lUsuarios=jdbcTemplate.queryForList(sql);
	   return lUsuarios;
	}

	@Override	
	public User GetUserById(int identificacion) 
	{
		
		String sql="SELECT ra.Id_llave,ra.Usuario,r.Id_rol,r.Nombre_rol,CASE WHEN ra.Estado=1 THEN 'Activo' ELSE 'Inactivo' END Estado " + 
				"FROM rol_acceso ra " + 
				"JOIN rol r ON ra.id_rol=r.Id_rol "+
				"WHERE ra.Id_llave = ?";
		
		return jdbcTemplate.queryForObject(sql, new GetUserIdRowMapper(),identificacion);
	    
	}
	
	class GetUserIdRowMapper implements RowMapper<User>
	{
		@Override
		public User mapRow(ResultSet rs,int i) throws  SQLException
		{
			User user=new User();
			user.setIdentificacion(rs.getInt("Id_llave"));
			user.setUsuario(rs.getString("Usuario"));
			user.setRol(rs.getString("Nombre_rol"));
			user.setIdRol(rs.getInt("Id_rol"));
			return user;
		}
	}
	@Override
	public boolean GuardarUsuario(User usuario)
	{	
		boolean guardo=false;
		String sql="INSERT INTO rol_acceso(Id_llave,Usuario,Contrasena,id_rol)\r\n" + 
				   "VALUES(" + usuario.Identificacion +",'" +usuario.Usuario +"','" + usuario.Contrasena + "','"+usuario.IdRol +"'"+")";
				  
		int registro=jdbcTemplate.update(sql);
		if(registro>0)
		{
			guardo=true;
		}
		return guardo;
	}



	@Override
	public boolean DeleteUser(int id) 
	{
		boolean deleteUser=false;
		String sql="UPDATE rol_acceso set estado=0 where Id_llave= "+ id;
		if(jdbcTemplate.update(sql)>0)
		{	
			deleteUser=true;
		}
		
		return deleteUser;		
			

	}



	@Override
	public boolean UpdateUser(User user)
	{
		boolean update=false;
		String sql= "UPDATE rol_acceso " + 
				    "SET Usuario ='" +user.Usuario +"'," + 
				    " Contrasena = '"+ user.Contrasena + "'," + 
				    " id_rol = "+ user.IdRol + ","+
				    " Estado = "+user.Estado +" "+
				    " WHERE Id_llave = "+ user.Identificacion;
				if(jdbcTemplate.update(sql)>0)
				{
					update=true;
				}
				return update;
	}



	
}
