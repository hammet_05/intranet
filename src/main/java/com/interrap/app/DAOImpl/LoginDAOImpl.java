package com.interrap.app.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.interrap.app.DAO.ILoginDAO;
import com.interrap.app.model.User;

@Repository
public class LoginDAOImpl implements ILoginDAO
{
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
	@Override
	public User Loguearse(User user)
	{
		
		String sql="SELECT * FROM intranet_lucy.rol_acceso where Usuario=? and Contrasena=?";
		
		return jdbcTemplate.queryForObject(sql, new UsuarioRowMapper(),user.Usuario,user.Contrasena);
	}

}
class UsuarioRowMapper implements RowMapper<User>
{
	@Override
	public User mapRow(ResultSet rs,int i) throws  SQLException
	{
		User user=new User();
		user.setUsuario(rs.getString("Usuario"));
		user.setIdRol(rs.getInt("id_rol"));
		return user;
	}
}