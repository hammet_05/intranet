package com.interrap.app.DAOImpl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.interrap.app.DAO.ICargoDAO;
import com.interrap.app.model.Conexion;

@Repository
public class CargosDAOImpl implements ICargoDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public CargosDAOImpl()
	{
		Conexion conex=new Conexion();
		this.jdbcTemplate=new JdbcTemplate(conex.Conectar());
	}

/*	@Override
	public List<String> getCargos() {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List<String> result = null;

		try {

			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from emp_cargo");	
			
			rs = ps.executeQuery();
			
			result = new Vector<String>();

			while(rs.next()) {
				result.add(rs.getString("cargo"));
			}			
			
			return result;

		} catch (SQLException e) {
			
			throw new RuntimeException(e);

		} finally {			
				try {
					
					if(rs!=null)
						rs.close();
					if(ps!=null)
						ps.close();
					if(conn!=null)
						conn.close();
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
		}
	}*/
	@Override
	public List<String>ObtenerCargos()
	{
		String sql="SELECT * FROM emp_cargo";
				  
		List lCargos=jdbcTemplate.queryForList(sql);
	   return lCargos;
	}
}
